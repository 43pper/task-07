package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static final String GET_ALL_USERS = "SELECT * FROM users";
	private static final String ADD_USER = "INSERT INTO users (login) VALUES (?)";
	private static final String DELETE_USER = "DELETE FROM users WHERE login = ?";
	private static final String GET_ALL_TEAMS = "SELECT * FROM teams";
	private static final String ADD_TEAM = "INSERT INTO teams (name) VALUES (?)";
	private static final String FIND_USER_BY_LOGIN = "SELECT * FROM users WHERE login = ?";
	private static final String GET_TEAM = "SELECT * FROM teams  WHERE name = ?";
	private static final String ADD_USER_TEAMS = "INSERT INTO users_teams (user_id, team_id) VALUES (?,?)";
	private static final String GET_ALL_USER_TEAMS = "SELECT  * FROM users_teams WHERE user_id =?";
	private static final String DELETE_TEAM = "DELETE FROM teams WHERE name = ?";
	private static final String UPDATE_TEAM = "UPDATE teams SET name =? WHERE id=?";
	private static final String GET_TEAM_BY_ID = "SELECT * FROM  teams WHERE id =?";


	private static final String CONNECTION_URL_PROPERTY_NAME = "connection.url";
	private static final String CONNECTION_PROPERTIES_FILE_NAME = "app.properties";

	private final String url;

	public DBManager() {

		Properties properties = new Properties();
		try (FileReader reader = new FileReader(CONNECTION_PROPERTIES_FILE_NAME)) {
			properties.load(reader);
			url = properties.getProperty(CONNECTION_URL_PROPERTY_NAME);
		} catch (IOException e) {
			e.printStackTrace(); // log
			throw new IllegalStateException(e);
		}

	}





	public static synchronized DBManager getInstance() {
		return new DBManager();
	}


	private Connection getConnection(boolean autocommit) throws DBException {
		Connection con = null;
		try {
			con = DriverManager.getConnection(url);
			con.setAutoCommit(autocommit);
			return con;
		}catch (SQLException e){
			throw new DBException("Cannot get connection", e);
		}

	}

	public List<User> findAllUsers() throws DBException  {
		try(Connection con = getConnection(true);
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery(GET_ALL_USERS)) {
			List<User> users = new ArrayList<>();
			while (rs.next()){
				User user = mapUser(rs);
				users.add(user);
			}
			return users;
		} catch (SQLException e){
			throw new DBException("Cannot get user", e);
		}
	}



	public boolean insertUser(User user) throws DBException {
		Connection con = null;
		try{
			con = getConnection(false);
			addUser(con, user);
			con.commit();
			return true;
		}  catch (Exception e) {
			throw new DBException("Cannot insert user", e.getCause());
		} finally {
			close(con);
		}
	}

	private static void close(Connection con) throws DBException {
		if(con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				throw new DBException("Cannot insert user", e.getCause());
			}
		}
	}

	private static void rollback(Connection con) throws DBException {
		if(con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				throw new DBException("DB trouble", e);
			}
		}
	}

	private void addUser(Connection con, User user) throws Exception {
		try (PreparedStatement statement = con.prepareStatement(ADD_USER, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			statement.setString(++i, user.getLogin());
			int c = statement.executeUpdate();
			if (c > 0) {
				try (ResultSet keys = statement.getGeneratedKeys()) {
					if (keys.next()) {
						user.setId(keys.getInt(1));

					}
				}
			}
		} catch (Exception ex) {
			throw new DBException("DB trouble", ex);
		}
	}


	public boolean deleteUsers(User... users) throws DBException {
		Connection connection = null;
		try {
			connection = getConnection(false);
			for (User user: users) {
				deleteUser(connection,user);
			}
			connection.commit();
			return true;
		} catch (SQLException e){
			rollback(connection);
			throw new DBException("Cannot delete user", e);
		} finally {
			close(connection);
		}
	}

	private void deleteUser(Connection connection, User user) throws DBException{
		try (PreparedStatement pr = connection.prepareStatement(DELETE_USER)){
			pr.setString(1,user.getLogin());
			pr.executeUpdate();
		} catch (SQLException e) {
			throw new DBException("Cannot get User", e);
		}
	}

	public User getUser(String login) throws DBException {
		try (Connection connection = getConnection(true);
			 PreparedStatement statement = connection.prepareStatement(FIND_USER_BY_LOGIN)){
			statement.setString(1,login);
			ResultSet set = statement.executeQuery();
			set.next();
			return mapUser(set);
		}catch (SQLException exception){
			throw new DBException("Cannot get User", exception);
		}
	}

	public Team getTeam(String name) throws DBException {
		try(Connection connection = getConnection(true);
			PreparedStatement statement = connection.prepareStatement(GET_TEAM)){
			statement.setString(1,name);
			ResultSet set = statement.executeQuery();
			set.next();
			return mapTeam(set);
		} catch (SQLException e){
			throw new DBException("Cannot get Team", e);
		}
	}

	public List<Team> findAllTeams() throws DBException {
		try (Connection connection = getConnection(true);
			 Statement statement = connection.createStatement();
			 ResultSet resultSet = statement.executeQuery(GET_ALL_TEAMS)){
			List<Team> teams = new ArrayList<>();
			while (resultSet.next()){
				Team team = mapTeam(resultSet);
				teams.add(team);
			}
			return teams;
		} catch (SQLException exception){
			throw new DBException("Cannot get team", exception);
		}
	}

	private Team mapTeam(ResultSet resultSet) throws SQLException {
		Team team = new Team();
		team.setName(resultSet.getString("name"));
		team.setId(resultSet.getInt("id"));
		return team;
	}

	public boolean insertTeam(Team team) throws DBException {
		Connection connection = null;
		try {
			connection = getConnection(false);
			addTeam(connection,team);
			connection.commit();
			return true;
		} catch (SQLException exception){
			rollback(connection);
			throw new DBException("cannot insert team", exception);
		} finally {
			close(connection);
		}
	}



	private void addTeam(Connection connection, Team team) throws SQLException, DBException {
		try(PreparedStatement statement = connection.prepareStatement(ADD_TEAM, Statement.RETURN_GENERATED_KEYS)) {
			int i = 0;
			statement.setString(++i, team.getName());
			int c = statement.executeUpdate();
			if( c > 0){
				try (ResultSet resultSet = statement.getGeneratedKeys()){
					if(resultSet.next()){
						team.setId(resultSet.getInt(1));
					}
				}
			}
		} catch(Exception ex) {
			throw new DBException("DB trouble", ex);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement stmt = null;
		try{
			con = getConnection(false);
			int count = 0;
			for (Team team : teams){
				try {
					stmt = con.prepareStatement(ADD_USER_TEAMS);
					int k = 0;
					stmt.setInt(++k, user.getId());
					stmt.setInt(++k, team.getId());
					count += stmt.executeUpdate();
				} catch (SQLException e) {
					rollback(con);
					throw new DBException("Transaction setTeamsForUsers failed", e.getCause());
				}
			}
			if (count == teams.length) con.commit();
			return true;
		}catch (Exception e){
			rollback(con);
			throw new DBException("Transaction setTeamsForUsers failed", e.getCause());
		}
		finally {
			close(con);
		}
	}

	private void setTeamForUser(Connection connection, User user, Team team) throws SQLException {
		try (PreparedStatement statement = connection.prepareStatement(ADD_USER_TEAMS)) {
			int i = 0;
			statement.setInt(++i, user.getId());
			statement.setInt(++i, team.getId());
			statement.executeUpdate();
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		try (Connection connection = getConnection(true);
			 PreparedStatement statement = connection.prepareStatement(GET_ALL_USER_TEAMS)){

			statement.setInt(1,user.getId());

			try (ResultSet set = statement.executeQuery();){
				List<Team> teams = new ArrayList<>();
				while (set.next()){

					Team team = mapTeamByID(connection,set);
					teams.add(team);
				}
				return teams;
			}
		} catch (SQLException ex){
			throw new DBException("Cannot found user teams", ex);
		}
	}

	private Team mapTeamByID(Connection connection, ResultSet set) throws DBException {
		try {
			int id = set.getInt("team_id");
			PreparedStatement statement = connection.prepareStatement(GET_TEAM_BY_ID);
			statement.setInt(1,id);
			ResultSet teamInfo = statement.executeQuery();
			teamInfo.next();
			Team team = new Team();
			team.setName(teamInfo.getString("name"));
			team.setId(teamInfo.getInt("id"));
			return team;
		} catch (SQLException e){
			throw new DBException("Cannot get team by id", e);
		}
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection connection = null;
		try {
			connection = getConnection(false);
			deleteTeamStmt(team, connection);
			connection.commit();
			return true;
		} catch (SQLException e){
			rollback(connection);
			throw new DBException("Cannot delete user", e);
		} finally {
			close(connection);
		}
	}

	private void deleteTeamStmt(Team team, Connection connection) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(DELETE_TEAM)){
			statement.setString(1, team.getName());
			statement.executeUpdate();
		} catch (SQLException e){
			throw new DBException("Cannot delete team in stmt", e);
		}

	}

	public boolean updateTeam(Team team) throws DBException {
		Connection connection = null;
		try {
			connection = getConnection(false);
			updateTeamInf(connection,team);
			connection.commit();
			return true;
		} catch (SQLException e){
			rollback(connection);
			throw new DBException("Cannot update team", e);
		}finally {
			close(connection);
		}
	}

	private void updateTeamInf(Connection connection, Team team) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(UPDATE_TEAM)){
			int i = 0;
			statement.setString(++i,team.getName());
			statement.setInt(++i,team.getId());
			statement.executeUpdate();
		} catch (SQLException exception){
			throw new DBException("Cannot update team", exception);
		}
	}

	private User mapUser(ResultSet rs) throws SQLException {
		User user = new User();
		user.setLogin(rs.getString("login"));
		user.setId(rs.getInt("id"));
		return user;
	}



}
