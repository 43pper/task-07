package com.epam.rd.java.basic.task7.db.entity;

public class User {

	public User(){};
	public User(String login){
		this.login = login;
		this.id = 0;
	}

	public User(int id, String login){
		setId(id);
		setLogin(login);
	}

	private int id;

	private String login;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {
		return new User(login);
	}

	public String toString(){
		return login;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof User) {
				return this.login.equals(((User) obj).login);
			}
		}
		return false;
	}
}
