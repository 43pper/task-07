package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	public Team(String name){
		setName(name);
		setId(0);
	}

	public Team(){}
	private int id;

	private String name;

	public Team(int id, String name) {
		setId(id);
		setName(name);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (obj instanceof Team) {
				return this.name.equals(((Team) obj).name);
			}
		}
		return false;
	}

	@Override
	public String toString(){
		return name;
	}
}


